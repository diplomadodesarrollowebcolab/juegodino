document.addEventListener('keydown', function(evento) {
    

 if(evento.keyCode == 32){
     console.log("salta: ");    
     if (tsub.y == suelo){
     saltar();
     
    }
    
      
 }
})

var imgSub, imgCrucero, imgTorpedo, imgMar;

function cargaImagenes(){
    imgSub = new Image();
    imgCrucero = new Image();
    imgTorpedo = new Image();
    imgTorpedo1 = new Image();
    imgMar = new Image();

    imgSub.src = 'imagenes/sub.png';
    imgCrucero.src = 'imagenes/crucero.jpg';
    imgTorpedo.src = 'imagenes/torpedo.jpg';
    imgTorpedo1.src = 'imagenes/torpedo1.jpg';
    imgMar.src = 'imagenes/mar1.jpg';
}

var ancho = 700;
var alto = 300;
//ctx (contexto) 
var canvas, ctx;

function inicializa(){
canvas = document.getElementById('canvas');
ctx = canvas.getContext('2d'); //el contexto es como funciona la pizarra de canvas en este caso una pantalla 2d
cargaImagenes();
}

//hay que hacer la funcion para borrar canvas por cada fotograma
function borrarCanvas(){
    canvas.width = ancho;
    canvas.height =alto;
} 

var suelo = 200;
// y es la posicion vertical del submarino en canvas. Total de altura menos el tamño del sub
// vy es la velocidad vertical que esta subiendo o bajando (cuanto pixeles tiene que poner o quitar)
// salto son los pixeles que subira de golpe al principio. Le diremos que se mueva a 28 pixeles por fotograma y cada que pase un F le restaremos 2
var tsub = {y: suelo ,vy: 1, gravedad:2, salto:22, vymax: 9, saltando:false}; 

function dibujarSubmarino(){
    ctx.drawImage(imgSub, 0,0,477,248,100,tsub.y,85,45); // se delcara la imagen, los 0 asi se quedan luego se indica el tamaño de la imagen, luego en donde se va a posicionar dentro de canvas (eje x e y) y luego si le queremos asignar otro tamaño a la imagen 
}
var nivel = {velocidad: 9, puntuacion:0};
var torpedo2 = {x:ancho+200, y: suelo};
var crucero = {x: ancho + 100, y: 100}; 
var mar = {x: 0, y: 0}; 

function dibujarTorpedo(){
ctx.drawImage(imgTorpedo,0,0,214,116, torpedo2.x, torpedo2.y+28, 50, 25);
}

function logicaTorpedo(){
if(torpedo2.x < -100){
    torpedo2.x = ancho + 100;
}
else{
    torpedo2.x -= nivel.velocidad;
}
}

function dibujarCrucero(){
    ctx.drawImage(imgCrucero,0,0,398,95, crucero.x, crucero.y-80, 200, 45);
    }

function logicaCrucero(){
  if(crucero.x < -200){
    crucero.x = ancho + 200;
  }
  else{
    crucero.x -= 5;
    }
}

function dibujarMar(){
    ctx.drawImage(imgMar,mar.x,0,1300,975, 0, 50, 1300, 250);
    }
    
function logicaMar(){
    if(mar.x > 700){
        mar.x = 0;
    }
    else{
        mar.x += nivel.velocidad;
    }
}

function saltar(){
    tsub.saltando = true;
    tsub.vy = tsub.salto;
}

function gravedad(){
if(tsub.saltando == true){

    if(tsub.y - tsub.vy - tsub.gravedad > suelo){
        tsub.saltando =false;
        tsub.vy= 0;
        tsub.y= suelo;
    }
    else{

    tsub.vy -= tsub.gravedad;
    tsub.y -= tsub.vy; //"25
    }
}
}

//Bucle principal
var FPS = 10;

setInterval(function(){
    principal(); //es un intervalo en el que la función principal se va a estar ejecutando 10 veces por segundo siempre
},1000/10);

//esta funcion va a ir llamando a todo el juego. Llevara las nubes, el dinosaurio el suelo y se repetira 10 veces en un segundo la funcion principaltodo 
function principal(){
    borrarCanvas();
    gravedad();
    logicaMar();
    dibujarMar();
    logicaCrucero();
    dibujarCrucero();
    logicaTorpedo();
    dibujarTorpedo();
    dibujarSubmarino();
} 
